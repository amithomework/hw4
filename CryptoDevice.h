#pragma once

#include <cstdio>
#include <iostream>
#include "osrng.h"
#include "modes.h"
#include <string.h>
#include <cstdlib>

using namespace std;


class CryptoDevice
{

public:
	CryptoDevice();
	~CryptoDevice();
	bool login(string,string);
	string encryptAES(string);
	string decryptAES(string);
private:
	map<string, string> secret;
};
