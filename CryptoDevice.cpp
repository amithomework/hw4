#include "CryptoDevice.h"

//aquire a default key for AES and block size for CBC. can't generate randomly, since all the agents shuold have the same key.
//can initialize with a unique sequence but not needed.
byte key[CryptoPP::AES::DEFAULT_KEYLENGTH], iv[CryptoPP::AES::BLOCKSIZE];


CryptoDevice::CryptoDevice()
{
	secret.insert(pair<string, string>("yoyo", "C3FCD3D76192E4007DFB496CCA67E13B")); //password abcdefghijklmnopqrstuvwxyz
	secret.insert(pair<string, string>("jons", "952B0A49BA1EB956626806738D59CD41")); //password hEy123
	secret.insert(pair<string, string>("bob", "1AB9984825E8CBE050BA1140E7DA358D")); //password copy_paste
	secret.insert(pair<string, string>("roll", "5F4DCC3B5AA765D61D8327DEB882CF99")); //password password
}
CryptoDevice::~CryptoDevice(){}

using namespace CryptoPP;

string CryptoDevice::encryptAES(string plainText)
{
	string cipherText;

	memset(key, 0, sizeof(key));
	memset(iv, 0, sizeof(iv));

	AES::Encryption aesEncyption(key, AES::DEFAULT_KEYLENGTH);
	CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncyption, iv);
	StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(cipherText));
	stfEncryptor.Put(reinterpret_cast<const unsigned char*>(plainText.c_str()), plainText.length());
	stfEncryptor.MessageEnd();
	
	return cipherText;
}

string CryptoDevice::decryptAES(string cipherText)
{

	string decryptedText, block;
	
	memset(key, 0, sizeof(key));
	memset(iv, 0, sizeof(iv));

	for (int i = 0; i < cipherText.length(); i += AES::DEFAULT_KEYLENGTH)
	{
		
		block = cipherText.substr(i, i + AES::DEFAULT_KEYLENGTH);
		cipherText.replace(i, i + AES::DEFAULT_KEYLENGTH, block);
		AES::Decryption aesDecryption(key, AES::DEFAULT_KEYLENGTH);
		CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);
		StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(decryptedText));
		stfDecryptor.Put(reinterpret_cast<const unsigned char*>(block.c_str()), AES::DEFAULT_KEYLENGTH);
		stfDecryptor.MessageEnd();
	}
	return decryptedText;
}

bool CryptoDevice::login(string userName, string password)
{
	auto it = secret.find(userName);
	if (it == secret.end())
		return false;
	if (it->second == password)
		return true;
	return false;
}
